package edu.laboral.test;

import static org.junit.Assert.assertEquals;

import java.util.Date;

import org.junit.Before;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import edu.laboral.dto.UsuarioResponseDTO;



public class UsuarioControllerTest extends AbstractTest {
	
	private static Logger log = LoggerFactory.getLogger(UsuarioControllerTest.class);
	
   @Override
   @Before
   public void setUp() {
      super.setUp();
   }
   
   @Test
   public void createProduct() throws Exception {
	   
      String uri = "/registro";
      UsuarioResponseDTO usuarioResponseDTO = new UsuarioResponseDTO();
      usuarioResponseDTO.setName("Eduardo Espinoza");
      usuarioResponseDTO.setCreated(new Date());
      usuarioResponseDTO.setEmail("skate1914@hotmail.com");
      
      String inputJson = super.mapToJson(usuarioResponseDTO);
      log.info("inputJson: "+inputJson);
      MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(uri)
         .contentType(MediaType.APPLICATION_JSON_VALUE)
         .content(inputJson)).andReturn();
      
      int status = mvcResult.getResponse().getStatus();
      assertEquals(200, status);
      String content = mvcResult.getResponse().getContentAsString();
      assertEquals(content, "Usuario is created successfully");
   }
   
}
