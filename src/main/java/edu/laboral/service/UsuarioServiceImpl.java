package edu.laboral.service;

import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import edu.laboral.dto.PhoneDTO;
import edu.laboral.dto.UsuarioRequestDTO;
import edu.laboral.dto.UsuarioResponseDTO;
import edu.laboral.entity.Usuario;
import edu.laboral.repository.UsuarioRepository;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;


@Service("usuarioService")
public class UsuarioServiceImpl implements UsuarioService{
	
	private static Logger logger = LoggerFactory.getLogger(UsuarioServiceImpl.class);
	
	@Autowired
	private UsuarioRepository usuarioRepository;
	
	public void existeEmail(UsuarioRequestDTO usuarioRequest) throws Exception {
		
		
		List<Usuario> listUsuarioEntity = usuarioRepository.findByEmail(usuarioRequest.getEmail());	
		
		if(listUsuarioEntity!=null && !listUsuarioEntity.isEmpty()) {
			
			throw new Exception("Correo ya existe");
		}
	}
	public List<UsuarioResponseDTO> massiveSelectUsuario(UsuarioRequestDTO usuarioRequest) {
		
		return null;
	}
	public UsuarioResponseDTO save(UsuarioRequestDTO usuarioRequest) {
		
		UsuarioResponseDTO usuarioResponse = new UsuarioResponseDTO();
		String token = null;
		try {
			
			Usuario usuarioEntity = new Usuario();
			usuarioEntity.setUsername(usuarioRequest.getName());
			usuarioEntity.setPassword(usuarioRequest.getPassword());
			token = getToken(usuarioRequest.getName());
			usuarioEntity.setToken(token);
			usuarioEntity.setName(usuarioRequest.getName());
			usuarioEntity.setIsactive("true");
			usuarioEntity.setEmail(usuarioRequest.getEmail());
			
			List<PhoneDTO> listPhones = new ArrayList<PhoneDTO>();
			for (PhoneDTO phoneDTO : usuarioRequest.getPhones()) {
				listPhones.add(phoneDTO);
			}
			
			usuarioRepository.save(usuarioEntity);
			
			
			usuarioResponse.setName(usuarioRequest.getName());
			usuarioResponse.setPassword(usuarioRequest.getPassword());
			usuarioResponse.setEmail(usuarioRequest.getEmail());
			usuarioResponse.setToken(token);
			usuarioResponse.setCreated(new Date());
			usuarioResponse.setModified(new Date());
			usuarioResponse.setLast_login(usuarioResponse.getCreated());
			usuarioResponse.setIsactive(true);
			usuarioResponse.setPhones(listPhones);
			
		} catch (UnsupportedEncodingException e) {
	
			e.printStackTrace();
		}
		return usuarioResponse;
		
	}
	
	private String getToken(String name) throws UnsupportedEncodingException {
		
		String jwt = Jwts.builder()

				  .setExpiration(new Date(1300819380))
				  .claim("name", name)

				  .signWith(
				    SignatureAlgorithm.HS256,
				    "secret".getBytes("UTF-8")
				  )
				  .compact();
		logger.info("Token: "+jwt);
		return jwt;
		
	}
	
	
}
