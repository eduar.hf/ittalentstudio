package edu.laboral.service;

import edu.laboral.dto.UsuarioRequestDTO;
import edu.laboral.dto.UsuarioResponseDTO;

public interface UsuarioService {
	
	void existeEmail(UsuarioRequestDTO usuarioRequest) throws Exception;
	UsuarioResponseDTO save(UsuarioRequestDTO usuarioRequest);
}
