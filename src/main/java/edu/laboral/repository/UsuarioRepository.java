package edu.laboral.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import edu.laboral.entity.Usuario;

public interface UsuarioRepository extends JpaRepository<Usuario, Long> {

	Usuario findByUsername(String username);
	List<Usuario> findByEmail(String email);
}
