package edu.laboral.controller;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import edu.laboral.dto.UsuarioRequestDTO;
import edu.laboral.dto.UsuarioResponseDTO;
import edu.laboral.service.UsuarioService;

@RestController
public class RegistroController {
	
	private static Logger log = LoggerFactory.getLogger(RegistroController.class);
	
	@Autowired 
	private UsuarioService usuarioService;
	
	@PostMapping("registro")
	@ResponseBody
	public ResponseEntity<UsuarioResponseDTO> registro(@Valid @RequestBody UsuarioRequestDTO usuarioRequest, HttpServletRequest req) throws Exception {
		
		log.info("endpoint registro iniciando...");
		
		try {
			
			usuarioService.existeEmail(usuarioRequest);
			
			UsuarioResponseDTO usuarioResponse = usuarioService.save(usuarioRequest);
			return new ResponseEntity<>(usuarioResponse, HttpStatus.OK);
			
		}catch(Exception e) {
			
			log.error(e.getMessage());
			throw new ResponseStatusException(HttpStatus.BAD_REQUEST, e.getMessage());
		}
		
	}	
}
