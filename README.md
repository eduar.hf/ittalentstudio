
## Api Creacion de Usuarios

## Ejecucion de aplicación localmente

Para iniciar la aplicación construida con Spring Boot
debe ejecutar método main de la clase <code>edu.laboral.Application</code> 
desde su IDE. 

## Ruta de api de creacion de usuarios:

<table>
<thead>
<tr>
<th>URL</th>
<th>Method</th>
<th>Remarks</th>
<th>Request Body</th>
</tr>
</thead>
<tbody>
<tr>
<td><code>http://localhost:8080/registro</code></td>
<td>POST</td>
<td></td>
<td>JSON</td>
</tr>
<tr>
</tbody>
</table>


<h5>Se adjunta datos de cuerpo de solicitud JSON válidos</h5>

<pre>
{
"name": "Juan Rodriguez",
"email": "juan@rodriguez.org",
"password": "Hunter22",
"phones": [
{
"number": "1234567",
"citycode": "1",
"contrycode": "57"
}
]
}
</pre>
